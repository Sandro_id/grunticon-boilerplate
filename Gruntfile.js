module.exports = function(grunt) {
	"use strict";

	// Project configuration.
	grunt.initConfig({
		grunticon: {
			foo: {
				options: {
					// required config
					src: "example/source/",
					dest: "example/output/",

					// optional grunticon config properties
					// SVGO compression, false is the default, true will make it so
					svgo: true,

					// PNG compression, true is the default, false will skip it
					pngcrush: true,

					// CSS filenames
					datasvgcss: "icons.data.svg.css",
					datapngcss: "icons.data.png.css",
					urlpngcss: "icons.fallback.css",

					// preview HTML filename
					previewhtml: "preview.html",

					// grunticon loader code snippet filename
					loadersnippet: "grunticon.loader.txt",

					// folder name (within dest) for png output
					pngfolder: "png",

					// prefix for CSS classnames
					cssprefix: "icon-",

					defaultWidth: "300px",
					defaultHeight: "200px",

					// define vars that can be used in filenames if desirable, like foo.colors-primary-secondary.svg
					colors: {
						primary: "red",
						secondary: "#666"
					},

					// css file path prefix - this defaults to "/" and will be placed before the "dest" path when stylesheets are loaded.
					// This allows root-relative referencing of the CSS. If you don't want a prefix path, set to to ""
					cssbasepath: "/",
					customselectors: {
						//"filename" : "selector"
						"cat" : "#el-gato",
						"gummy-bears-2" : "nav li a.deadly-bears:before"
					}

				}
			}
		}
	});
	
	grunt.loadNpmTasks('grunt-grunticon');
	// Default task.
	grunt.registerTask('default', ['grunticon:foo']);
};
