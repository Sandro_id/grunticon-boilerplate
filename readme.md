# Grunt Boilerplate + Grunticon

## Getting started with Grunt #

See the [Getting Started](http://gruntjs.com/getting-started "Getting started") instructions on gruntjs.com.

Grunt is dependent on [Node.js](http://nodejs.org/ "Node.js") and uses the [Node Package Manager](https://npmjs.org/ "Node Package Manager") so download and install Node.js if you don't already have it.

Install [grunt-cli](https://github.com/gruntjs/grunt-cli) and you'll have access to the grunt command anywhere on your system.

```js
npm install -g grunt-cli
```

## Checkout the Repo

```js
git clone https://Sandro_id@bitbucket.org/Sandro_id/grunticon-boilerplate.git
```

## install dependencies from package.json 
```js
npm install 
```

## start grunt to build svg-icon-spritesheet

```js
grunt
```
